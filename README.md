![Microsoft PowerShell](https://www.gravatar.com/userimage/148298775/73fc5d28ce1b57b27c59929acbc1377b?size=48)

## *Microsoft PowerShell ToolSet*

*Windows PowerShell 工具脚本集*

----

1. **开发环境介绍**
    1. IDE: *Microsoft Visual Studio Code* & *Microsoft PowerShell ISE*
    2. NuGet [CommandLine Tools][NuGet-CommandLineTools]
2. **脚本工具集功能介绍**
    1. *Modules*

        | Module Name         | Documentation Link                                    | Features           |
        | ------------------- | ----------------------------------------------------- | ------------------ |
        | PSCommandFuctionSet | [Click & View](./docs/Modules/PSCommonFunctionSet.md) | *公共方法集*       |
        | NuCommandSet        | [Click & View](./docs/Modules/NuCommandSet.md)        | *NuGet 公共方法集* |
3. **安装部署到 Windows**
    1. 找到系统环境变量: ***%PSModulePath%***
    2. 系统全局安装
        1. 将 Modules 文件夹的所有内容复制到环境变量 ***%PSModulePath%*** 指定的略经中。
        2. 自定义路径
            1. 将自定义路径 (比如 D:\MyPSModules) 添加到系统环境变量 ***%PSModulePath%*** 中。
    3. 使用 [PowerShellGet](https://www.powershellgallery.com/packages/PowerShellGet/2.0.3) 进行安装
        1. Install-Module -Name *[NetOwls.PSCommonFunctionSet](https://www.powershellgallery.com/packages/NetOwls.PSCommonFunctionSet/2018.11.27.145152)*
        2. Install-Module -Name *[NetOwls.NuCommandSet](https://www.powershellgallery.com/packages/NetOwls.NuCommandSet/2018.11.26.131450)*

> Links
>
> [![][GithubIcon]][Github] [![][GitlabIcon]][Gitlab]

[NuGet-CommandLineTools]: https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
[Github]: https://github.com/NetOwls/PowerShell-ToolSet
[GithubIcon]: https://www.gravatar.com/userimage/148298775/3861daaa66c8420fc971e782c6332461?size=16
[Gitlab]: https://gitlab.com/netowls/powershell-toolset
[GitlabIcon]: https://www.gravatar.com/userimage/148298775/a47618b805a461cf72313233824140a8?size=16