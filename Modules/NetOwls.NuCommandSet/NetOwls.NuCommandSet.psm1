﻿# -----------------------------------------------------------------------------------------------------
# FEATURE: 提供了与 NUGET 命令行相关的脚本模块。
# AUTHOR: WANG YUCAI
# CREATED TIME: 2018/11/26 10:58
# CODE FILE: NETOWLS.NUCOMMANDSET.PSM1
# E-MAIL ADDRESS: NETOWLS@OUTLOOK.COM
# COPYRIGHT © 2006 - 2018 WANG YUCAI. ALL RIGHTS RESERVED.
# -----------------------------------------------------------------------------------------------------

data LocalizationData {
    ConvertFrom-StringData @'
		NuCliNotExistPromptMessage=The nuget command-line tool not exists. We will download it. Please wait several minutes.
		VSProjectPathNotFound=The VisualStudio Project was not found: {0}
'@
}

# 资源引入
Import-LocalizedData LocalizationData -FileName "NuCommandSetResources"

# NuGet 命令行工具路径。
$NuRootDir = ".\NuGet"
# NuGet 命令行工具下载地址模板。
$NuCliUriTemplate = "https://dist.nuget.org/win-x86-commandline/{0}/nuget.exe"

<#
	.SYNOPSIS
		从官网下载指定版本的 NuGet 命令行工具。
	
	.DESCRIPTION
		从官网下载指定版本的 NuGet 命令行工具。
	
	.PARAMETER LastestVersion
		是否需要下载最新版本的 NuGet 命令行工具。默认为 True。
	
	.PARAMETER VersionNumber
		指定的 NuGet 命令行工具版本号数字。此参数仅当 LastestVersion 等于 False 时有效。
#>
function InternalGet-NuCommandLineTools {
    [CmdletBinding(HelpUri = 'https://github.com/NetOwls/PowerShell-ToolSet/blob/master/docs/Modules/NuCommandSet.md')]
    param
    (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            Position = 0,
            HelpMessage = '是否需要下载最新版本的 NuGet 命令行工具。默认为 True。')]
        [Alias('Lastest')]
        [bool]$LastestVersion = $true,
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $false,
            ValueFromPipelineByPropertyName = $false,
            Position = 1,
            HelpMessage = '指定的 NuGet 命令行工具版本号数字。此参数仅当 LastestVersion 等于 False 时有效。')]
        [Alias('Ver')]
        [string]$VersionNumber
    )
	
    $TargetUri = $NuCliUriTemplate
    If (($LastestVersion) -or ([string]::IsNullOrWhiteSpace($VersionNumber))) {
        $TargetUri = $TargetUri -f "latest"
    }
    Else {
        $TargetUri = $TargetUri -f $VersionNumber
    }
	
    If (-not (Test-Path -Path $NuRootDir)) {
        mkdir -Path $NuRootDir
    }
    If (-not (Test-Path -Path ("{0}\nuget.exe" -f $NuRootDir))) {
        Log-Warning -Message ("{0} (URI: {1})" -f $LocalizationData.NuCliNotExistPromptMessage, $TargetUri)
        wget -Uri $TargetUri -OutFile ("{0}\nuget.exe" -f $NuRootDir)
    }
}

<#
	.SYNOPSIS
		从 NuGet.org 官网下载指定版本的命令行工具。
	
	.DESCRIPTION
		从 NuGet.org 官网下载指定版本的命令行工具。
	
	.PARAMETER LastestRelease
		仅需要下载最新的版本。默认为 True。
	
	.PARAMETER SpecialVersionNumber
		指定的版本号。此参数仅在 LastestRelease 等于 False 启用。
#>
function Get-NuCommandLineTools {
    [CmdletBinding(HelpUri = 'https://github.com/NetOwls/PowerShell-ToolSet/blob/master/docs/Modules/NuCommandSet.md')]
    param
    (
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 0,
            HelpMessage = '仅需要下载最新的版本。默认为 True。')]
        [Alias('Lastest')]
        [bool]$LastestRelease = $true,
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 1,
            HelpMessage = '指定的版本号。此参数仅在 LastestRelease 等于 False 启用。')]
        [Alias('Version')]
        [string]$SpecialVersionNumber = "lastest"
    )
	
    InternalGet-NuCommandLineTools -Lastest $LastestRelease -VersionNumber $SpecialVersionNumber
}
Export-ModuleMember -Function Get-NuCommandLineTools

# Optional commands to create a public alias for the function
New-Alias -Name wget-nucli -Value Get-NuCommandLineTools
Export-ModuleMember -Alias wget-nucli

<#
	.SYNOPSIS
		调用 NuGet.exe 命令行工具，为指定项目创建 NuGet Special 文件 (*.nuspec)。
	
	.DESCRIPTION
		调用 NuGet.exe 命令行工具，为指定项目创建 NuGet Special 文件 (*.nuspec)。
	
	.PARAMETER ProjectFilePath
		指定的 Visual Studio 项目文件路径。
	
	.NOTES
		Additional information about the function.
#>
function Create-NuSpecials {
    [CmdletBinding(HelpUri = 'https://github.com/NetOwls/PowerShell-ToolSet/blob/master/docs/Modules/NuCommandSet.md')]
    param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 0,
            HelpMessage = '指定的 Visual Studio 项目文件路径。')]
        [Alias('Proj')]
        [string]$ProjectFilePath
    )
	
    InternalGet-NuCommandLineTools -LastestVersion $true
	
    If (-not [string]::IsNullOrWhiteSpace($ProjectFilePath)) {
        If (Test-Path -Path $ProjectFilePath) {
            . ("{0}\nuget.exe" -f $NuRootDir) "spec" $ProjectFilePath "-Force"
        }
        Else {
            Log-Error -Message ($LocalizationData.VSProjectPathNotFound -f $ProjectFilePath)
        }
    }
}
Export-ModuleMember -Function Create-NuSpecials

# Optional commands to create a public alias for the function
New-Alias -Name nu-spec -Value Create-NuSpecials
Export-ModuleMember -Alias nu-spec

<#
	.SYNOPSIS
		创建一个 NuGet 包。
	
	.DESCRIPTION
		创建一个 NuGet 包。
	
	.PARAMETER ProjectBasePath
		Visual Studio 项目(或者 *.nuspec 文件)的基础路径。
	
	.PARAMETER Suffix
		是否使用 Debug 版本进行打包。
	
	.PARAMETER OutputDirectory
		NuGet 包输出路径。
	
	.PARAMETER UseDebug
		使用 Debug 版本。
	
	.PARAMETER Surfix
		包的后缀。
#>
function Create-NuPackage {
    [CmdletBinding(HelpUri = 'https://github.com/NetOwls/PowerShell-ToolSet/blob/master/docs/Modules/NuCommandSet.md')]
    param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 0,
            HelpMessage = 'Visual Studio 项目(或者 *.nuspec 文件)的基础路径。')]
        [Alias('BasePath')]
        [string]$ProjectBasePath,
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 1,
            HelpMessage = '是否使用 Debug 版本进行打包。')]
        [ValidateSet('None', 'Debug', 'RC', 'Alpha', 'Beta', 'Develop', 'Nightly', 'Daily', 'Weekly', IgnoreCase = $false)]
        [string]$Suffix = "None",
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 3,
            HelpMessage = 'NuGet 包输出路径。')]
        [Alias('OutDir')]
        [string]$OutputDirectory,
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 2,
            HelpMessage = '使用 Debug 版本。')]
        [switch]$UseDebug
    )
	
    If ([string]::IsNullOrWhiteSpace($OutputDirectory)) {
        $OutputDirectory = ".\"
    }
    If (-not (Test-Path -Path $OutputDirectory)) {
        mkdir -Path $OutputDirectory
    }
    If ($UseDebug.ToBool()) {
        . ("{0}\nuget.exe" -f $NuRootDir) "pack" $ProjectBasePath "-IncludeReferencedProjects" "-Properties" "Configuration=Debug" "-Suffix" "Debug" "-OutputDirectory" $OutputDirectory
    }
    Else {
        switch ($Suffix) {
            "None" {
                . ("{0}\nuget.exe" -f $NuRootDir) "pack" $ProjectBasePath "-IncludeReferencedProjects" "-Properties" "Configuration=Release" "-OutputDirectory" $OutputDirectory
            }
            default {
                . ("{0}\nuget.exe" -f $NuRootDir) "pack" $ProjectBasePath "-IncludeReferencedProjects" "-Properties" "Configuration=Release" "-Suffix" $Suffix "-OutputDirectory" $OutputDirectory
            }
        }
    }
}


Export-ModuleMember -Function Create-NuPackage

# Optional commands to create a public alias for the function
New-Alias -Name nu-pack -Value Create-NuPackage
Export-ModuleMember -Alias nu-pack

<#
	.SYNOPSIS
		发布一个 NuGet 包到指定的源。
	
	.DESCRIPTION
		发布一个 NuGet 包到指定的源。
	
	.PARAMETER PackageName
		指定的包文件(*.nupkg)名称。
	
	.PARAMETER ApiKey
		NuGet 服务所需的标识。
	
	.PARAMETER SourceUris
		发布到的源地址。
#>
function Publish-NuPackage {
    [CmdletBinding(HelpUri = 'https://github.com/NetOwls/PowerShell-ToolSet/blob/master/docs/Modules/NuCommandSet.md')]
    param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 0,
            HelpMessage = '指定的包文件(*.nupkg)名称。')]
        [Alias('NuPkgFile')]
        [string]$PackageName,
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 1,
            HelpMessage = 'NuGet 服务所需的标识。')]
        [string]$ApiKey,
        [Parameter(Mandatory = $false,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 2,
            HelpMessage = '发布到的源地址。')]
        [Alias('Sources')]
        [string[]]$SourceUris
    )
	
    If ((-not [string]::IsNullOrWhiteSpace($PackageName)) -and (Test-Path -Path $PackageName)) {
        InternalGet-NuCommandLineTools -LastestVersion $true
		
        If ($SourceUris -eq $null) {
            $SourceUris = @()
        }
        If ($SourceUris.Length -eq 0) {
            $SourceUris += "https://api.nuget.org/v3/index.json"
        }
		
        If ([string]::IsNullOrWhiteSpace($ApiKey)) {
            foreach ($item in $SourceUris) {
                . ("{0}\nuget.exe" -f $NuRootDir) "push" $PackageName "-Source" $item
            }
        }
        else {
            foreach ($item in $SourceUris) {
                . ("{0}\nuget.exe" -f $NuRootDir) "push" $PackageName "-Source" $item "-ApiKey" $ApiKey
            }
        }
    }
}
Export-ModuleMember -Function Publish-NuPackage

# Optional commands to create a public alias for the function
New-Alias -Name nu-push -Value Publish-NuPackage
Export-ModuleMember -Alias nu-push


# SIG # Begin signature block
# MIIWFgYJKoZIhvcNAQcCoIIWBzCCFgMCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQU16fjebQ4NN0GH7cvkRy74O03
# RQSggg9bMIIEFDCCAvygAwIBAgILBAAAAAABL07hUtcwDQYJKoZIhvcNAQEFBQAw
# VzELMAkGA1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExEDAOBgNV
# BAsTB1Jvb3QgQ0ExGzAZBgNVBAMTEkdsb2JhbFNpZ24gUm9vdCBDQTAeFw0xMTA0
# MTMxMDAwMDBaFw0yODAxMjgxMjAwMDBaMFIxCzAJBgNVBAYTAkJFMRkwFwYDVQQK
# ExBHbG9iYWxTaWduIG52LXNhMSgwJgYDVQQDEx9HbG9iYWxTaWduIFRpbWVzdGFt
# cGluZyBDQSAtIEcyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlO9l
# +LVXn6BTDTQG6wkft0cYasvwW+T/J6U00feJGr+esc0SQW5m1IGghYtkWkYvmaCN
# d7HivFzdItdqZ9C76Mp03otPDbBS5ZBb60cO8eefnAuQZT4XljBFcm05oRc2yrmg
# jBtPCBn2gTGtYRakYua0QJ7D/PuV9vu1LpWBmODvxevYAll4d/eq41JrUJEpxfz3
# zZNl0mBhIvIG+zLdFlH6Dv2KMPAXCae78wSuq5DnbN96qfTvxGInX2+ZbTh0qhGL
# 2t/HFEzphbLswn1KJo/nVrqm4M+SU4B09APsaLJgvIQgAIMboe60dAXBKY5i0Eex
# +vBTzBj5Ljv5cH60JQIDAQABo4HlMIHiMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMB
# Af8ECDAGAQH/AgEAMB0GA1UdDgQWBBRG2D7/3OO+/4Pm9IWbsN1q1hSpwTBHBgNV
# HSAEQDA+MDwGBFUdIAAwNDAyBggrBgEFBQcCARYmaHR0cHM6Ly93d3cuZ2xvYmFs
# c2lnbi5jb20vcmVwb3NpdG9yeS8wMwYDVR0fBCwwKjAooCagJIYiaHR0cDovL2Ny
# bC5nbG9iYWxzaWduLm5ldC9yb290LmNybDAfBgNVHSMEGDAWgBRge2YaRQ2XyolQ
# L30EzTSo//z9SzANBgkqhkiG9w0BAQUFAAOCAQEATl5WkB5GtNlJMfO7FzkoG8IW
# 3f1B3AkFBJtvsqKa1pkuQJkAVbXqP6UgdtOGNNQXzFU6x4Lu76i6vNgGnxVQ380W
# e1I6AtcZGv2v8Hhc4EvFGN86JB7arLipWAQCBzDbsBJe/jG+8ARI9PBw+DpeVoPP
# PfsNvPTF7ZedudTbpSeE4zibi6c1hkQgpDttpGoLoYP9KOva7yj2zIhd+wo7AKvg
# IeviLzVsD440RZfroveZMzV+y5qKu0VN5z+fwtmK+mWybsd+Zf/okuEsMaL3sCc2
# SI8mbzvuTXYfecPlf5Y1vC0OzAGwjn//UYCAp5LUs0RGZIyHTxZjBzFLY7Df8zCC
# BJ8wggOHoAMCAQICEhEh1pmnZJc+8fhCfukZzFNBFDANBgkqhkiG9w0BAQUFADBS
# MQswCQYDVQQGEwJCRTEZMBcGA1UEChMQR2xvYmFsU2lnbiBudi1zYTEoMCYGA1UE
# AxMfR2xvYmFsU2lnbiBUaW1lc3RhbXBpbmcgQ0EgLSBHMjAeFw0xNjA1MjQwMDAw
# MDBaFw0yNzA2MjQwMDAwMDBaMGAxCzAJBgNVBAYTAlNHMR8wHQYDVQQKExZHTU8g
# R2xvYmFsU2lnbiBQdGUgTHRkMTAwLgYDVQQDEydHbG9iYWxTaWduIFRTQSBmb3Ig
# TVMgQXV0aGVudGljb2RlIC0gRzIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
# AoIBAQCwF66i07YEMFYeWA+x7VWk1lTL2PZzOuxdXqsl/Tal+oTDYUDFRrVZUjtC
# oi5fE2IQqVvmc9aSJbF9I+MGs4c6DkPw1wCJU6IRMVIobl1AcjzyCXenSZKX1GyQ
# oHan/bjcs53yB2AsT1iYAGvTFVTg+t3/gCxfGKaY/9Sr7KFFWbIub2Jd4NkZrItX
# nKgmK9kXpRDSRwgacCwzi39ogCq1oV1r3Y0CAikDqnw3u7spTj1Tk7Om+o/SWJMV
# TLktq4CjoyX7r/cIZLB6RA9cENdfYTeqTmvT0lMlnYJz+iz5crCpGTkqUPqp0Dw6
# yuhb7/VfUfT5CtmXNd5qheYjBEKvAgMBAAGjggFfMIIBWzAOBgNVHQ8BAf8EBAMC
# B4AwTAYDVR0gBEUwQzBBBgkrBgEEAaAyAR4wNDAyBggrBgEFBQcCARYmaHR0cHM6
# Ly93d3cuZ2xvYmFsc2lnbi5jb20vcmVwb3NpdG9yeS8wCQYDVR0TBAIwADAWBgNV
# HSUBAf8EDDAKBggrBgEFBQcDCDBCBgNVHR8EOzA5MDegNaAzhjFodHRwOi8vY3Js
# Lmdsb2JhbHNpZ24uY29tL2dzL2dzdGltZXN0YW1waW5nZzIuY3JsMFQGCCsGAQUF
# BwEBBEgwRjBEBggrBgEFBQcwAoY4aHR0cDovL3NlY3VyZS5nbG9iYWxzaWduLmNv
# bS9jYWNlcnQvZ3N0aW1lc3RhbXBpbmdnMi5jcnQwHQYDVR0OBBYEFNSihEo4Whh/
# uk8wUL2d1XqH1gn3MB8GA1UdIwQYMBaAFEbYPv/c477/g+b0hZuw3WrWFKnBMA0G
# CSqGSIb3DQEBBQUAA4IBAQCPqRqRbQSmNyAOg5beI9Nrbh9u3WQ9aCEitfhHNmmO
# 4aVFxySiIrcpCcxUWq7GvM1jjrM9UEjltMyuzZKNniiLE0oRqr2j79OyNvy0oXK/
# bZdjeYxEvHAvfvO83YJTqxr26/ocl7y2N5ykHDC8q7wtRzbfkiAD6HHGWPZ1BZo0
# 8AtZWoJENKqA5C+E9kddlsm2ysqdt6a65FDT1De4uiAO0NOSKlvEWbuhbds8zkSd
# wTgqreONvc0JdxoQvmcKAjZkiLmzGybu555gxEaovGEzbM9OuZy5avCfN/61PU+a
# 003/3iCOTpem/Z8JvE3KGHbJsE2FUPKA0h0G9VgEB7EYMIIGnDCCBISgAwIBAgIB
# bzANBgkqhkiG9w0BAQ0FADCB1zELMAkGA1UEBhMCQ0gxEDAOBgNVBAgTB0JlaWpp
# bmcxEDAOBgNVBAcTB0hhaWRpYW4xETAPBgNVBAoTCFBlcnNvbmFsMRIwEAYDVQQL
# EwlXYW5nWXVjYWkxIjAgBgkqhkiG9w0BCQEWE05ldE93bHNAb3V0bG9vay5jb20x
# GzAZBgNVBAUTEjExMDIyMTE5ODMwOTE1ODMxMTEXMBUGA1UEDBMOLk5FVCBEZXZl
# bG9wZXIxDjAMBgNVBAQTBVl1Y2FpMRMwEQYDVQQpEwpXYW5nIFl1Y2FpMB4XDTE4
# MDkxNDA1MjEwMFoXDTIwMDkxNDA1MjEwMFowgdcxCzAJBgNVBAYTAkNIMRAwDgYD
# VQQIEwdCZWlqaW5nMRAwDgYDVQQHEwdIYWlkaWFuMREwDwYDVQQKEwhQZXJzb25h
# bDESMBAGA1UECxMJV2FuZ1l1Y2FpMSIwIAYJKoZIhvcNAQkBFhNOZXRPd2xzQG91
# dGxvb2suY29tMRswGQYDVQQFExIxMTAyMjExOTgzMDkxNTgzMTExFzAVBgNVBAwT
# Di5ORVQgRGV2ZWxvcGVyMQ4wDAYDVQQEEwVZdWNhaTETMBEGA1UEKRMKV2FuZyBZ
# dWNhaTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAMIZsqv4W1xxyimF
# 3Rs3a7gbo38NktB59MJAPUDFezw+hCvVr6jZNW2sMKq/lZNScVSKEOPAPHFAq2Db
# hUcThdmwzexyD0T1VwDZwtpetibUZ34LWjdDLmoYOmLbd9M//x9tkoE4sEaT/DBq
# mbS6YWWLsWmWvpGyYNFv5pWM+p2mMULlPwX5SSY/rbgCwLcWknQQFl6ZOCmp0JW1
# ozxpwEv6iF75gfIL69CXZRKdjf1wmMC2hlAKfPSm1kGcXzce1CVowAqj0Pc2wa1I
# cXz+z/CcGt0TuIcKkmtHxJDbBPZTfkGI/Emkva/mtHUPTZaTM9yfD1uBUJaMb/xE
# RLg5TdxdkzWjfCc7n1ZGSmedg6UWJQVLywU+tBuO4ggR7iIpEA3z7YjrlkIS2xPn
# UXoWQs1okqJBpemeNMG1suc4gS3pHObtbsR+avjM2bNN2xTOzWYi4OyegS0rpVDp
# vEGMSAtQXe9j10W9CHxZhrOcBdRzwcpdMT34MjG7p9cxDmbsA/K0YsAQmroPfQ1b
# BaD5CCpo+xsbipI5NPPxPIr079bmJBIeAQQX29waMRVfoHiP4zsdq6f5YQ0FVXnb
# cIsUJtDZvZWGB5NlNeva22F+9AAnTCWolz6X/D9bO3963Yd9g484InnmJmjqmaow
# H7gKZ7ZNu4qh6MW9fnIa85G2BLZJAgMBAAGjcTBvMAwGA1UdEwQFMAMBAf8wDwYD
# VR0PAQH/BAUDAwcBgDAuBgNVHSUBAf8EJDAiBggrBgEFBQcDAwYKKwYBBAGCNwIB
# FQYKKwYBBAGCNwIBFjAeBglghkgBhvhCAQ0EERYPeGNhIGNlcnRpZmljYXRlMA0G
# CSqGSIb3DQEBDQUAA4ICAQCzJcuYq20ELsCgWQF4O3YHrltZ+2jzFySNhHgd1D/l
# S+s//aNztHqiUXgTMCOHnKN8vl60mUMloPDP6mb0fgUPZxM4L6T1aiuH63kvHXLZ
# 5VMdJ6L1l0qu2hXUyP7y0ap3tlgM6NyYwkUWTeKydYNZQZg7lJ9D1BBEoaGj95WM
# Cs9sAm9eX53ZX5eJOuhhjUAU2lqY9cTUqTq2FHwSpzqJ/KoOA2bg9d/Zr1lZU18Z
# ASN+PND34Ri+EPRFuKPWdXomt1YLMewuEJ/C4IOvjxbKMbCERG/UAvJz+TZfOqrn
# Dqn4O52y+iVPnWxalolU2b1VWzq35tGvJKO8SWMxvDtr39lUauxmdZ3xc1Sbp80O
# xa2Bzb10mr0a8YzL8pGTq9fBxV/6A8OPdJVKqw+vgwRofT0cYkh4O3YwNxawdGqS
# mrpTefIg8PGOLqlPquUVjvgiYNTo8m3chzaFAuVP92o/QlUX+CEr+fvjgU+wxjJX
# w8rhPXpdz9nGRuq1+6FFFggK+Lid5uTVP4BwKaxSy+4Zmir6LZUOVk7GmkDor8Jp
# oxgpV4dkI6E/063g1Ti2yjNxyG8IFhHPwH5asNkMDeeJcbHJdxVniO/7BlghtLgF
# uQp5IZeEe0kBuLmsm41+RaeRWzdCHJyuAb4q15SpB6VMby1AEicb/ju2VGN9CJXo
# HDGCBiUwggYhAgEBMIHdMIHXMQswCQYDVQQGEwJDSDEQMA4GA1UECBMHQmVpamlu
# ZzEQMA4GA1UEBxMHSGFpZGlhbjERMA8GA1UEChMIUGVyc29uYWwxEjAQBgNVBAsT
# CVdhbmdZdWNhaTEiMCAGCSqGSIb3DQEJARYTTmV0T3dsc0BvdXRsb29rLmNvbTEb
# MBkGA1UEBRMSMTEwMjIxMTk4MzA5MTU4MzExMRcwFQYDVQQMEw4uTkVUIERldmVs
# b3BlcjEOMAwGA1UEBBMFWXVjYWkxEzARBgNVBCkTCldhbmcgWXVjYWkCAW8wCQYF
# Kw4DAhoFAKB4MBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZIhvcNAQkD
# MQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARYwIwYJ
# KoZIhvcNAQkEMRYEFBWyBpb8M369e7SVCqmK2o3l1u77MA0GCSqGSIb3DQEBAQUA
# BIICAJiXAPao9bfM4EGMzaBEDv4dc9Y7NHwxoiyo2RqQZQP2YcU90ZtawPSeqzBw
# j8nNBMs+uc1oukgBqyNKGY0Stkhwo4wAo/8g1E9GjifEZS8HZJwJDwbkkkDf4zaq
# r7jNZJdASyD5M7QXX6mo0M4dyMTkXa59QpT0wUD5uY3i8boEnmN9voNKwyuL1oct
# UiHHmfztxcf48pxi3gujx/Qyn9wTsks8J6Ldum20VYlkYr9aMt4mdKsztvSsYCDZ
# ljMdoym5HSf3pnYCg1UQj/ARUXrH723VXNp8NnEZJp0ZSiL96c4w2n39op46PD4h
# lxas3YQ3z7CqEXPpOVFu+DkC/knQLdNcKKHcO7Qzc4y55jYUrfT5R1R6kBNdnwKU
# g66Aov4lTtVRFwYg03P4V+Sppk/caseMDci3uGwgnHPM1895vthwtBOUPDnSzHx1
# IzHTwMd7oylHGl0dLOMPqLjGxJ8VUliL3Waa2CoTolDKIRxEAAwwwk0oMsq7HaVo
# YrYtPczLtJ93zdwEkcQE+koSb6a1h73JltWyJLXGbmwa/Gb2OwhfpgJezGeqacT1
# ksQLhivmcaQhQ4I2b2aRuWeQlKbqIW9PPcyRbdOB8f7Z28kmVDqyRfLFW+VdmCJO
# 3PNEeXZebpMqzO/9pYzqbT7XPpTTKgjYydiHc8RRDPs2sTJNoYICojCCAp4GCSqG
# SIb3DQEJBjGCAo8wggKLAgEBMGgwUjELMAkGA1UEBhMCQkUxGTAXBgNVBAoTEEds
# b2JhbFNpZ24gbnYtc2ExKDAmBgNVBAMTH0dsb2JhbFNpZ24gVGltZXN0YW1waW5n
# IENBIC0gRzICEhEh1pmnZJc+8fhCfukZzFNBFDAJBgUrDgMCGgUAoIH9MBgGCSqG
# SIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTE4MTEyNjA4NDcz
# MlowIwYJKoZIhvcNAQkEMRYEFMcFNP9jmVEA+ESETPVOsqrzH/0NMIGdBgsqhkiG
# 9w0BCRACDDGBjTCBijCBhzCBhAQUY7gvq2H1g5CWlQULACScUCkz7HkwbDBWpFQw
# UjELMAkGA1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExKDAmBgNV
# BAMTH0dsb2JhbFNpZ24gVGltZXN0YW1waW5nIENBIC0gRzICEhEh1pmnZJc+8fhC
# fukZzFNBFDANBgkqhkiG9w0BAQEFAASCAQBeyQVv0t33f73skhtX7oiWWgTpoWzj
# kz148quDO+iysjvegEOj5t4XDhI4VubwZgi6AR5JBR+41zYmZxLtl5a0OyjgjoFQ
# nr6XTJwzRnwFQxk6w9YC3iL6ONgoq4VcEU1p57RddR2zu1A4gvQTBkEf/pYFg1NS
# D875F4hm01o2nNDMvtxo2pHfWyvEkc0qrItJCXPjxCrooCdMxZMP037KPCMhpTeu
# LgrZYCBTjpFEowNYiqnRNxa/33l9M7zox0ajMs5VLnZdtA7aak4kuUrpEp24Mb0Q
# mBa+FO3BsMzw0BoTS/LAMBmHypafuU7dLxrRYCwA/j5AwbG1aLNJsXSP
# SIG # End signature block
