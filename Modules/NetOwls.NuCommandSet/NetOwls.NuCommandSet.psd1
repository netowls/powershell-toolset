﻿# -----------------------------------------------------------------------------------------------------
# FEATURE: NETOWLS.NUCOMMANDSET 模块定义文件。
# AUTHOR: WANG YUCAI
# CREATED TIME: 2018/11/26 13:14
# CODE FILE: NETOWLS.NUCOMMANDSET.PSD1
# E-MAIL ADDRESS: NETOWLS@OUTLOOK.COM
# COPYRIGHT © 2006 - 2018 WANG YUCAI. ALL RIGHTS RESERVED.
# -----------------------------------------------------------------------------------------------------

@{

    # Filename (including extension) of primary script or binary module file. (Renamed to RootModule in PowerShell 3.0.)
    ModuleToProcess        = 'NetOwls.NuCommandSet.psm1'

    # Version number of this module. Increment with each release. Enter a string that can be converted to a System.Version object, e.g. 1.0.0.0. 
    ModuleVersion          = '2018.11.27.160904'

    # ID used to uniquely identify this module. # Do not increment between versions.
    GUID                   = 'd6f74c60-5c36-453f-a23a-9182de0e6e12'

    # Name, contact info (Twitter handle, email address) of the person responsible for this module.
    Author                 = 'Wang Yucai'

    # Name of the company or vendor that developed the module.
    CompanyName            = 'Personal'

    # Copyright statement for this module
    Copyright              = 'Copyright © 2006 - 2018 Wang Yucai. All rights reserved.'

    # Brief description of the module features.
    Description            = '提供了与 NuGet 相关的命令集。'

    # Enables updatable help. Enter the URL of the HelpInfoXML file for the module.
    # Valid in PowerShell 3.0 and later.
    #HelpInfoUri = ''


    #-----------------------------------------------------------
    #  Module requirements: 
    #     If PowerShell cannot verify/load these elements, attempts to import the module fail.

    # Minimum version of the common language runtime (CLR) required by this module. Valid only for Desktop edition.
    # CLRVersion = '2.0.50727'

    # Minimum version of the .NET Framework required by this module. Valid only for Desktop edition.
    DotNetFrameworkVersion = '4.5'

    # Name of the PowerShell host required by this module, such as 'ConsoleHost'  or 'Windows PowerShell ISE Host'
    # PowerShellHostName = 'ConsoleHost'

    # Minimum version of the PowerShell host required by this module
    # PowerShellHostVersion = '2.0'

    # Minimum version of the PowerShell engine required by this module
    # PowerShellVersion = '2.0'

    # Processor architecture (None, X86, Amd64, IA64) required by this module
    # ProcessorArchitecture = 'None'

    # Module runs only on particular edition of PowerShell: Desktop, Core. Valid only on PowerShell 5.1+
    #CompatiblePSEditions = 'Desktop', 'Core'

    # Assemblies that must be loaded prior to importing this module
    # RequiredAssemblies = @()

    # List names, paths, or ModuleSpecificaton objects. If PowerShell can't import the required modules, attempts to import this module fail.
    RequiredModules        = @("NetOwls.PSCommonFunctionSet")

    #-----------------------------------------------------------
    #-----------------------------------------------------------
    # What command types does the module export? 
    #       The *ToExport keys filter the result of the Export-ModuleMember cmdlet; they do not override it.
    #       For best performance, list names, not wildcards. Do not delete theses keys or value. 
    #       If the module does not export commands of this type, use an empty array value ( @() ). 

    # Names of aliases that the module exports. For best performance, list names, not wildcards, and do not delete this key or its value. 
    AliasesToExport        = @("wget-nucli", "nu-spec", "nu-pack", "nu-push")

    # Names of functions that the module exports or @(). For best performance, list names, not wildcards, and do not delete this key or its value. 
    FunctionsToExport      = @("Get-NuCommandLineTools", "Create-NuSpecials", "Create-NuPackage", "Publish-NuPackage")

    # Names of cmdlets that the module exports or @(). For best performance, list names, not wildcards, and do not delete this key or its value.
    CmdletsToExport        = @()

    # Names of variables to export from this module, all ('*') or none ( @() ). 
    VariablesToExport      = '*'

    # Names of DSC resources that the module exports or @(). For best performance, list names, not wildcards, and do not delete this key or its value.
    # Valid in PowerShell 4.0+.
    DscResourcesToExport   = @()

    #-------------------------------------------------------------
    #-------------------------------------------------------------
    #    What types of files are included in this module?
    #        Enter the path and filename (with extension) in a comma-separated list.

    # Modules included in this module package. List names, paths, or ModuleSpecification objects.
    # To create a manifest module, include the root module in this value.
    #NestedModules = @()

    # Format files (.ps1xml) that PowerShell loads when importing this module
    #FormatsToProcess = @()

    # Scripts (.ps1) that PowerShell runs in the caller's environment before importing this module.
    #ScriptsToProcess = @()

    # Type files (.ps1xml) that PowerShell loads when importing this module.
    #TypesToProcess = @()

    # Modules packaged with this module. List names, file paths, or ModuleSpecification objects.
    #ModuleList = @()

    # List of all files packaged with this module
    #FileList = @()


    #-------------------------------------------------------------
    #-------------------------------------------------------------
    # Use this key to send this arbitrary data (strings, hashtables) to the ModuleToProcess module. To get the data in your module functions, use $MyInvocation.MyCommand.Module.PrivateData.
    PrivateData            = @{

        PSData = @{

            # Keywords that identify or describe the module. Tags are used to find modules in online galleries.
            Tags       = @("NuGet")

            #  URL of the module license
            LicenseUri = 'https://github.com/NetOwls/PowerShell-ToolSet/blob/master/LICENSE.md'

            # URL of the main website for this project.
            ProjectUri = 'https://github.com/NetOwls/PowerShell-ToolSet'

            # URL of the icon that represents this module.
            IconUri    = 'https://www.gravatar.com/userimage/148298775/3e9fe66d1bb077d20eccca97dd42581b?size=120'

            # URL to the change log for the module or a list of changes to each version of the module.
            # ReleaseNotes = ''

        } # End of PSData hashtable

    } # End of PrivateData hashtable

}
# SIG # Begin signature block
# MIIKtQYJKoZIhvcNAQcCoIIKpjCCCqICAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUzO99BPeNe8yO4Sg0iT6GhCm6
# 69KgggagMIIGnDCCBISgAwIBAgIBbzANBgkqhkiG9w0BAQ0FADCB1zELMAkGA1UE
# BhMCQ0gxEDAOBgNVBAgTB0JlaWppbmcxEDAOBgNVBAcTB0hhaWRpYW4xETAPBgNV
# BAoTCFBlcnNvbmFsMRIwEAYDVQQLEwlXYW5nWXVjYWkxIjAgBgkqhkiG9w0BCQEW
# E05ldE93bHNAb3V0bG9vay5jb20xGzAZBgNVBAUTEjExMDIyMTE5ODMwOTE1ODMx
# MTEXMBUGA1UEDBMOLk5FVCBEZXZlbG9wZXIxDjAMBgNVBAQTBVl1Y2FpMRMwEQYD
# VQQpEwpXYW5nIFl1Y2FpMB4XDTE4MDkxNDA1MjEwMFoXDTIwMDkxNDA1MjEwMFow
# gdcxCzAJBgNVBAYTAkNIMRAwDgYDVQQIEwdCZWlqaW5nMRAwDgYDVQQHEwdIYWlk
# aWFuMREwDwYDVQQKEwhQZXJzb25hbDESMBAGA1UECxMJV2FuZ1l1Y2FpMSIwIAYJ
# KoZIhvcNAQkBFhNOZXRPd2xzQG91dGxvb2suY29tMRswGQYDVQQFExIxMTAyMjEx
# OTgzMDkxNTgzMTExFzAVBgNVBAwTDi5ORVQgRGV2ZWxvcGVyMQ4wDAYDVQQEEwVZ
# dWNhaTETMBEGA1UEKRMKV2FuZyBZdWNhaTCCAiIwDQYJKoZIhvcNAQEBBQADggIP
# ADCCAgoCggIBAMIZsqv4W1xxyimF3Rs3a7gbo38NktB59MJAPUDFezw+hCvVr6jZ
# NW2sMKq/lZNScVSKEOPAPHFAq2DbhUcThdmwzexyD0T1VwDZwtpetibUZ34LWjdD
# LmoYOmLbd9M//x9tkoE4sEaT/DBqmbS6YWWLsWmWvpGyYNFv5pWM+p2mMULlPwX5
# SSY/rbgCwLcWknQQFl6ZOCmp0JW1ozxpwEv6iF75gfIL69CXZRKdjf1wmMC2hlAK
# fPSm1kGcXzce1CVowAqj0Pc2wa1IcXz+z/CcGt0TuIcKkmtHxJDbBPZTfkGI/Emk
# va/mtHUPTZaTM9yfD1uBUJaMb/xERLg5TdxdkzWjfCc7n1ZGSmedg6UWJQVLywU+
# tBuO4ggR7iIpEA3z7YjrlkIS2xPnUXoWQs1okqJBpemeNMG1suc4gS3pHObtbsR+
# avjM2bNN2xTOzWYi4OyegS0rpVDpvEGMSAtQXe9j10W9CHxZhrOcBdRzwcpdMT34
# MjG7p9cxDmbsA/K0YsAQmroPfQ1bBaD5CCpo+xsbipI5NPPxPIr079bmJBIeAQQX
# 29waMRVfoHiP4zsdq6f5YQ0FVXnbcIsUJtDZvZWGB5NlNeva22F+9AAnTCWolz6X
# /D9bO3963Yd9g484InnmJmjqmaowH7gKZ7ZNu4qh6MW9fnIa85G2BLZJAgMBAAGj
# cTBvMAwGA1UdEwQFMAMBAf8wDwYDVR0PAQH/BAUDAwcBgDAuBgNVHSUBAf8EJDAi
# BggrBgEFBQcDAwYKKwYBBAGCNwIBFQYKKwYBBAGCNwIBFjAeBglghkgBhvhCAQ0E
# ERYPeGNhIGNlcnRpZmljYXRlMA0GCSqGSIb3DQEBDQUAA4ICAQCzJcuYq20ELsCg
# WQF4O3YHrltZ+2jzFySNhHgd1D/lS+s//aNztHqiUXgTMCOHnKN8vl60mUMloPDP
# 6mb0fgUPZxM4L6T1aiuH63kvHXLZ5VMdJ6L1l0qu2hXUyP7y0ap3tlgM6NyYwkUW
# TeKydYNZQZg7lJ9D1BBEoaGj95WMCs9sAm9eX53ZX5eJOuhhjUAU2lqY9cTUqTq2
# FHwSpzqJ/KoOA2bg9d/Zr1lZU18ZASN+PND34Ri+EPRFuKPWdXomt1YLMewuEJ/C
# 4IOvjxbKMbCERG/UAvJz+TZfOqrnDqn4O52y+iVPnWxalolU2b1VWzq35tGvJKO8
# SWMxvDtr39lUauxmdZ3xc1Sbp80Oxa2Bzb10mr0a8YzL8pGTq9fBxV/6A8OPdJVK
# qw+vgwRofT0cYkh4O3YwNxawdGqSmrpTefIg8PGOLqlPquUVjvgiYNTo8m3chzaF
# AuVP92o/QlUX+CEr+fvjgU+wxjJXw8rhPXpdz9nGRuq1+6FFFggK+Lid5uTVP4Bw
# KaxSy+4Zmir6LZUOVk7GmkDor8JpoxgpV4dkI6E/063g1Ti2yjNxyG8IFhHPwH5a
# sNkMDeeJcbHJdxVniO/7BlghtLgFuQp5IZeEe0kBuLmsm41+RaeRWzdCHJyuAb4q
# 15SpB6VMby1AEicb/ju2VGN9CJXoHDGCA38wggN7AgEBMIHdMIHXMQswCQYDVQQG
# EwJDSDEQMA4GA1UECBMHQmVpamluZzEQMA4GA1UEBxMHSGFpZGlhbjERMA8GA1UE
# ChMIUGVyc29uYWwxEjAQBgNVBAsTCVdhbmdZdWNhaTEiMCAGCSqGSIb3DQEJARYT
# TmV0T3dsc0BvdXRsb29rLmNvbTEbMBkGA1UEBRMSMTEwMjIxMTk4MzA5MTU4MzEx
# MRcwFQYDVQQMEw4uTkVUIERldmVsb3BlcjEOMAwGA1UEBBMFWXVjYWkxEzARBgNV
# BCkTCldhbmcgWXVjYWkCAW8wCQYFKw4DAhoFAKB4MBgGCisGAQQBgjcCAQwxCjAI
# oAKAAKECgAAwGQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIB
# CzEOMAwGCisGAQQBgjcCARYwIwYJKoZIhvcNAQkEMRYEFGpgCMxjlyrYEI6oUjNA
# PEBi7ZtZMA0GCSqGSIb3DQEBAQUABIICAAAhkLf7lCsXanibyM/xIfWUD1RwHG18
# c+hjYXGGM3bBORaF5YVa7N9w61Q0K5gT/VZSPJ9+c6Gp1kvpkZV5ED6u7ShYgmP5
# gCIO5zTYQttvqEtapblR+p9pzyBS6m54vNHr4aHStWfZfyz/l3j0ezrwyPD8oVa3
# n9jVwUHrH2GdHAA28al3ZRgO7/PDWRUJTcU4Ll5cuE6qwFw2si4FgKhF1J/NKIMk
# USnmRVjm6Mzd7N+KYfsBndkcicfrtmYTuQ+vMrokvr3b5RG9M4AR9nudyZ1+XHxm
# CWfLK5s0sYYdywuPK3/mvbQKNcuDQcwD00n/SA1z8RYBT7bmGkyhr67NXY+Tdrh2
# loI2yQl+Ot4CWU0zsu1XekM4i9+pJaY5uVl/Xsxg05rB0W5BDXOSxvTlrZrHSJ0m
# F5IDdR7zkhrZo1zYaFLf8PrpjD1M8qlSbJxHK28+jzzAJfBN+Kt6A/mazqEpkxM2
# uoaUNDCrAgGgVJ176AO5ooOkcUV+s76Z9HsvGBs4rGzGmhpUNigw9HN+9j3Iv9h4
# XAI42ioJO9KdUI0llpbJuBfut0Ap87LrnONxgFSxbeSrfduIOAfoGXNyzr+dacO2
# d9HoCe9/wGT8u6ZUyrS9jfVNCEDQ5NgGo1y1x2a9BffJSjO/SyMDWWsSHUpUctAP
# zslubtpbJNIg
# SIG # End signature block
