![Microsoft PowerShell](https://www.gravatar.com/userimage/148298775/73fc5d28ce1b57b27c59929acbc1377b?size=48)

## PSCommonFunctionSet Module

*[View Sources][Source]*

----

1. **功能说明**
    1. *PowerShell 控制台诊断信息输出*
        1. Log-Information 函数
            1. *别名：* log-info
            2. *功能：* 用于在 PowerShell 控制台中输出一行普通信息级诊断信息。
            3. *参数说明：*

                | 参数名           | 参数类型 | 别名  | 是否必选 | 功能说明                         | 默认值 |
                | ---------------- | -------- | ----- | -------- | -------------------------------- | ------ |
                | -Message         | String   | -Info | No       | 需要输出到控制台的诊断信息       | $null  |
                | -OutFileRequired | Boolean  |       | No       | 是否需要将诊断信息同时输出到文件 | $false |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.PSCommonFunctionSet
                PS > Log-Information -Message "This is a test information!" -OutFileRequired $true
                ```
        2. Log-Debug 函数
            1. *别名：* 无
            2. *功能：* 用于在 PowerShell 控制台中输出一行调试性诊断信息。
            3. *参数说明：*

                | 参数名   | 参数类型 | 别名  | 是否必选 | 功能说明                   | 默认值 |
                | -------- | -------- | ----- | -------- | -------------------------- | ------ |
                | -Message | String   | -Info | No       | 需要输出到控制台的诊断信息 | $null  |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.PSCommonFunctionSet
                PS > Log-Debug -Message "This is a test debug-information!"
                ```

        3. Log-Trace 函数
            1. *别名：* 无
            2. *功能：* 用于在 PowerShell 控制台中输出一行跟踪性诊断信息。
            3. *参数说明：*

                | 参数名   | 参数类型 | 别名  | 是否必选 | 功能说明                   | 默认值 |
                | -------- | -------- | ----- | -------- | -------------------------- | ------ |
                | -Message | String   | -Info | No       | 需要输出到控制台的诊断信息 | $null  |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.PSCommonFunctionSet
                PS > Log-Trace -Message "This is a test trace-information!"
                ```                

        4. Log-Warning 函数
            1. *别名：* log-warn
            2. *功能：* 用于在 PowerShell 控制台中输出一行警告信息级诊断信息。
            3. *参数说明：*

                | 参数名           | 参数类型 | 别名  | 是否必选 | 功能说明                         | 默认值 |
                | ---------------- | -------- | ----- | -------- | -------------------------------- | ------ |
                | -Message         | String   | -Warn | No       | 需要输出到控制台的诊断信息       | $null  |
                | -OutFileRequired | Boolean  |       | No       | 是否需要将诊断信息同时输出到文件 | $true  |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.PSCommonFunctionSet
                PS > Log-Warning -Message "This is a test warning!" -OutFileRequired $false
                ```

        5. Log-Error 函数
            1. *别名：* 无
            2. *功能：* 用于在 PowerShell 控制台中输出一行异常、错误信息级诊断信息。
            3. *参数说明：*

                | 参数名           | 参数类型 | 别名   | 是否必选 | 功能说明                         | 默认值 |
                | ---------------- | -------- | ------ | -------- | -------------------------------- | ------ |
                | -Message         | String   | -Error | No       | 需要输出到控制台的诊断信息       | $null  |
                | -OutFileRequired | Boolean  |        | No       | 是否需要将诊断信息同时输出到文件 | $true  |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.PSCommonFunctionSet
                PS > Log-Error -Message "The power-shell throw an unhandled exception!"
                ``` 
        6. Prompt-ChoiseOptions
            1. *别名：* prompt-choise
            2. *功能：* 用于在 PowerShell 控制台中输出几个备选项。用于交互。
            3. *参数说明：*

                | 参数名         | 参数类型 | 别名        | 是否必选 | 功能说明         | 默认值                            |
                | -------------- | -------- | ----------- | -------- | ---------------- | --------------------------------- |
                | -Prompt        | String   | -Msg        | No       | 提示信息         | *$LocalizationData.DefaultPrompt* |
                | -Options       | String[] |             | No       | 备选项字符串数组 | $null                             |
                | -DefaultOption | Int32    | -DefaultOpt | No       | 默认选择项序号   | 0                                 |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.PSCommonFunctionSet
                PS > $IChoise = Prompt-ChoiseOptions -Prompt "是否需要下载 NuGet 命令行工具?" -Options ("立即下载(&Download)", "暂不下载(&No)") -DefaultOption 1
                PS > $IChoise
                ```

2. **技术支持**
    1. *本地化支持*

        | Language Supported         | Status |
        | -------------------------- | ------ |
        | Simplified Chinese (zh-CN) | Done   |
        | English USA (en-US)        | Done   |

    *&copy; 2018 Wang Yucai.*               

[Source]: ../../Modules/NetOwls.PSCommonFunctionSet/NetOwls.PSCommonFunctionSet.psm1