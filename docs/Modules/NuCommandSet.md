![Microsoft PowerShell](https://www.gravatar.com/userimage/148298775/73fc5d28ce1b57b27c59929acbc1377b?size=48)

## NuCommandSet Module (for [NuGet.org](https://www.nuget.org))

*[View Sources](../../Modules/NetOwls.NuCommandSet/NetOwls.NuCommandSet.psm1)*

----

1. **功能说明**
    1. *NuGet 命令行*
        1. 模块依赖
            1. **[NetOwls.PSCommonFunctionSet](./PSCommonFunctionSet.md)**
        2. Get-NuCommandLineTools 函数
            1. *别名：* wget-nucli
            2. *功能：* 用于从 NuGet.org 下载指定版本的 NuGet 命令行工具。
            3. *参数说明：*

                | 参数名                | 参数类型 | 别名     | 是否必选 | 功能说明                                                                                      | 默认值   |
                | --------------------- | -------- | -------- | -------- | --------------------------------------------------------------------------------------------- | -------- |
                | -LastestRelease       | Boolean  | -Lastest | No       | 是否仅需要最新发布版本                                                                        | $true    |
                | -SpecialVersionNumber | String   | -Version | No       | 指定的 NuGet 命令行工具版本号 (例如：*v4.8.1*)。此参数仅在 LastestRelease 等于 False 时有效。 | "latest" |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.NuCommandSet
                PS > wget-nucli -LastestRelease $false -SpecialVersionNumber "v4.8.1"
                ```
        3. Create-NuSpecials 函数
            1. *别名：* nu-spec
            2. *功能：* 调用 NuGet 命令行 Spec 命令，创建包自述配置文件。
            3. *参数说明：*

                | 参数名           | 参数类型 | 别名  | 是否必选 | 功能说明                     | 默认值 |
                | ---------------- | -------- | ----- | -------- | ---------------------------- | ------ |
                | -ProjectFilePath | String   | -Proj | Yes      | Visual Studio 项目文件路径。 |        |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.NuCommandSet
                PS > nu-spec -ProjectFilePath "D:\Github.com\Entlib\src\EnterpriseLibrary.Common\EnterpriseLibrary.Common.csproj"
                ```
        4. Create-NuPackage 函数
            1. *别名：* nu-pack
            2. *功能：* 调用 NuGet 命令行 Pack 命令，创建包文件。
            3. *参数说明：*

                | 参数名           | 参数类型           | 别名      | 是否必选 | 功能说明                     | 默认值            |
                | ---------------- | ------------------ | --------- | -------- | ---------------------------- | ----------------- |
                | -ProjectBasePath | String             | -BasePath | Yes      | Visual Studio 项目文件路径。 |                   |
                | -Suffix          | String             |           | None     | NuGet 包后缀名称。           | "None"            |
                | -UseDebug        | *Switch Parameter* |           | None     | 是否启用 Debug 版本打包。    |                   |
                | -OutputDirectory | String             |           | -OutDir  | NuGet 打包完成后的输出路径。 | ".\" *(当前路径)* |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.NuCommandSet
                PS > cd "D:\Github.com\Entlib\src\EnterpriseLibrary.Common"
                PS > nu-pack -ProjectBasePath "." -Suffix "Nightly" -UseDebug $false -OutputDirectory "D:\NuGet"
                ```
        5. Publish-NuPackage 函数
            1. *别名：* nu-push
            2. *功能：* 调用 NuGet 命令行 Push 命令，发布包到指定 NuGet 源。
            3. *参数说明：*

                | 参数名       | 参数类型 | 别名       | 是否必选 | 功能说明                    | 默认值                                                                     |
                | ------------ | -------- | ---------- | -------- | --------------------------- | -------------------------------------------------------------------------- |
                | -PackageName | String   | -NuPkgFile | Yes      | NuGet 包文件(*.nupkg)路径。 |                                                                            |
                | -ApiKey      | String   |            | No       | NuGet 服务所需的发布密钥。  |                                                                            |
                | -Sources     | String[] |            | No       | NuGet 所需的包源路径。      | [https://api.nuget.org/v3/index.json](https://api.nuget.org/v3/index.json) |
            4. *Example：*

                ```
                [PowerShell]
                PS > Import-Module NetOwls.NuCommandSet
                PS > nu-push -PackageName "EnterpriseLibrary.Common.2018.11.26.165224.nupkg" -ApiKey "MyGet ApiKey" -Sources ("http://MyGet.com")
                ```

2. **技术支持**
    1. *本地化支持*

        | Language Supported         | Status |
        | -------------------------- | ------ |
        | Simplified Chinese (zh-CN) | Done   |
        | English USA (en-US)        | Done   |

    *&copy; 2018 Wang Yucai.*               